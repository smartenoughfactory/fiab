# Factory in a Box

Docker-compose.yml and supporting directories/files for the Factory in a Box application framework.  Clone into ~/fiab

_Note that the pi and chromebook frameworks (rpi, rpi_dev and chrome) are branches and need to be_ checked out _by git_

